#include <iostream>
#include "miniz.h"

int main() {
    auto compressor = tdefl_compressor{};
    auto flag = TDEFL_WRITE_ZLIB_HEADER | TDEFL_GREEDY_PARSING_FLAG | TDEFL_FORCE_ALL_RAW_BLOCKS;

    tdefl_init(&compressor, nullptr, nullptr, flag);

    return 0;
}
