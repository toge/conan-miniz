import shutil

from conans import CMake, ConanFile, tools

class MinizConan(ConanFile):
    name        = "miniz"
    version     = "2.1.0"
    license     = "MIT"
    author      = "toge.mail@gmail.com"
    url         = "https://bitbucket.org/toge/conan-miniz"
    description = "Single C source file zlib-replacement library, originally from code.google.com/p/miniz"
    topics      = ("compress", "zlib", "zip")
    settings    = "os", "compiler", "build_type", "arch"
    generators  = "cmake"

    options = {
        "shared"            : [True, False],
        "no_stdio"          : [True, False],
        "no_time"           : [True, False],
        "no_archive"        : [True, False],
        "no_archive_writing": [True, False],
        "no_zlib"           : [True, False],
        "no_zlib_compat"    : [True, False],
        "no_malloc"         : [True, False],
    }
    default_options = {
        "shared"            : False,
        "no_stdio"          : False,
        "no_time"           : False,
        "no_archive"        : False,
        "no_archive_writing": False,
        "no_zlib"           : False,
        "no_zlib_compat"    : False,
        "no_malloc"         : False,
    }


    def source(self):
        tools.get("https://github.com/richgel999/miniz/archive/{}.zip".format(self.version))
        shutil.move("miniz-{}".format(self.version), "miniz")

    def build(self):
        cmake = CMake(self)
        cmake.definitions["BUILD_EXAMPLES"] = False
        cmake.definitions["BUILD_SHARED_LIBS"] = self.options.shared

        cflags = list()
        if self.options.no_stdio:
            cflags.append("-DMINIZ_NO_STDIO")
        if self.options.no_time:
            cflags.append("-DMINIZ_NO_TIME")
        if self.options.no_archive:
            cflags.append("-DMINIZ_NO_ARCHIVE_APIS")
        if self.options.no_archive_writing:
            cflags.append("-DMINIZ_NO_ARCHIVE_WRITING_APIS")
        if self.options.no_zlib:
            cflags.append("-DMINIZ_NO_ZLIB_APIS")
        if self.options.no_zlib_compat:
            cflags.append("-DMINIZ_NO_ZLIB_COMPATIBLE_NAMES")
        if self.options.no_malloc:
            cflags.append("-DMINIZ_NO_MALLOC")
        if len(cflags) > 0:
            cmake.definitions["CONAN_C_FLAGS"] = " ".join(cflags)

        cmake.configure(source_folder="miniz")
        cmake.build()

    def package(self):
        self.copy("*.h",     dst="include", src="miniz")
        self.copy("*.lib",   dst="lib",     keep_path=False)
        self.copy("*.dll",   dst="bin",     keep_path=False)
        self.copy("*.so",    dst="lib",     keep_path=False)
        self.copy("*.dylib", dst="lib",     keep_path=False)
        self.copy("*.a",     dst="lib",     keep_path=False)

    def package_info(self):
        self.cpp_info.libs = ["miniz"]
